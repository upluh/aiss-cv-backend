from flask import Flask, render_template, request
import sqlite3
import os
import csv

current_directory = os.path.dirname(os.path.abspath(__file__))
database_file = os.path.join(current_directory, "lego.db")

# Connect to the database
conn = sqlite3.connect(database_file)

# Create a cursor
cur = conn.cursor()

cur.execute("DROP TABLE IF EXISTS states_assembly")
cur.execute("DROP TABLE IF EXISTS states_disassembly")
cur.execute("DROP TABLE IF EXISTS parts")
cur.execute("DROP TABLE IF EXISTS states_parts")

# Create tables
cur.execute(
    """CREATE TABLE IF NOT EXISTS states_assembly (
    name TEXT PRIMARY KEY,
    instruction TEXT
) """
)

cur.execute(
    """CREATE TABLE IF NOT EXISTS states_disassembly (
    name TEXT PRIMARY KEY,
    instruction TEXT
) """
)

cur.execute(
    """CREATE TABLE IF NOT EXISTS parts (
    name TEXT PRIMARY KEY,
    quantity INT
) """
)

cur.execute(
    """CREATE TABLE IF NOT EXISTS states_parts (
    state TEXT,
    part TEXT,
    quantity INT,
    PRIMARY KEY (state, part),
    FOREIGN KEY (state) REFERENCES states_assembly(name),
    FOREIGN KEY (state) REFERENCES states_disassembly(name),
    FOREIGN KEY (part) REFERENCES parts(name)
) """
)


with open("csv/states_assembly.csv", "r") as file:
    reader = csv.reader(file)
    # Skip the header row if it exists
    next(reader)

    # Iterate over each row in the CSV file
    for row in reader:
        # Extract the values from the row
        name = row[0]
        instructions = row[1]

        # Insert the values into the table
        cur.execute(
            """
            INSERT INTO states_assembly (name, instruction)
            VALUES (?, ?)
        """,
            (name, instructions),
        )

with open("csv/states_disassembly.csv", "r") as file:
    reader = csv.reader(file)
    # Skip the header row if it exists
    next(reader)

    # Iterate over each row in the CSV file
    for row in reader:
        # Extract the values from the row
        name = row[0]
        instructions = row[1]

        # Insert the values into the table
        cur.execute(
            """
            INSERT INTO states_disassembly (name, instruction)
            VALUES (?, ?)
        """,
            (name, instructions),
        )

with open("./csv/parts.csv", "r") as file:
    reader = csv.reader(file)
    # Skip the header row if it exists
    next(reader)

    # Iterate over each row in the CSV file
    for row in reader:
        # Extract the values from the row
        name = row[0]
        quantity = int(row[1])

        # Insert the values into the table
        cur.execute(
            """
            INSERT INTO parts (name, quantity)
            VALUES (?, ?)
        """,
            (name, quantity),
        )

with open("./csv/states_parts.csv", "r") as file:
    reader = csv.reader(file, delimiter="\t")
    # Skip the header row if it exists
    next(reader)

    # Iterate over each row in the CSV file
    for row in reader:
        # Extract the values from the row
        state = row[0]
        part = row[1]
        quantity = int(row[2])

        # Insert the values into the table
        cur.execute(
            """
            INSERT INTO states_parts (state, part, quantity)
            VALUES (?, ?, ?)
        """,
            (state, part, quantity),
        )


conn.commit()

# Close the database objects
cur.close()
conn.close()
