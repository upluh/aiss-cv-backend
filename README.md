# Artifical Intelligence in Service Systems - Oracle

The AISCV CV LEGO server is a RESTful backend application designed to facilitate the assembly and disassembly of a LEGO robot using computer vision (CV). This backend is a part of a prototype for a computer vision machine learning system, which aids users in ensuring they have the right LEGO parts at each step of the assembly or disassembly process.

**Functionality & Features**

1. **Welcome Endpoint**:
   - Route: `/`
   - Method: `GET`
   - Response: Welcomes users to the AISCV CV LEGO server.
2. **Instruction Endpoint**:
   - Route: `/instruction`
   - Method: `GET`
   - Parameters:
     - `state`: Represents the current state of the LEGO assembly or disassembly.
     - `assemblyMode`: Determines whether the instruction is for assembly or disassembly.
   - Response: Based on the current state and mode, the backend provides the next instruction from the database, as well as the relevant parts and quantities needed for that instruction.
3. **Full Set Check Endpoint**:
   - Route: `/full-set-check`
   - Method: `POST`
   - Body: The user provides a list of identified parts using computer vision.
   - Response: The backend verifies if the user has a complete set of LEGO parts, indicates which parts are missing, and provides the average confidence level of the identified parts.
4. **State Part Check Endpoint**:
   - Route: `/state-part-check`
   - Method: `POST`
   - Parameters:
     - `state`: Represents the current state of the LEGO assembly.
   - Body: The user provides a list of identified parts using computer vision.
   - Response: The backend checks if the user has the required parts for the current state of assembly, indicates which parts are missing, and provides the average confidence level of the identified parts.

**Backend Components & Implementation Details**:

1. **Database**: The backend utilizes an SQLite database (`lego.db`), which contains tables that map states to instructions and relevant parts.
2. **CORS**: The application uses the `CORS` module, which allows the server to handle cross-origin requests. This is useful when the frontend and backend are hosted on different domains.
3. **JSON Handling**: The server is designed to handle input and output in JSON format, making it easy to integrate with other services or applications.
4. **Error Handling**: The server gracefully handles potential errors, ensuring that meaningful responses are sent to the client even when something goes wrong.
5. **Utility Functions**:
   - `full_set_calculator()`: Calculates if the user has the full set of LEGO parts.
   - `check_parts_of_states()`: Checks if the user has the necessary parts for a specific state of assembly.
   - `convert_input_to_json()`: Converts the received input to a standard JSON format for easier processing.

**Context**:
The primary objective of this backend server is to assist users in building a LEGO robot. With the integration of computer vision, users can determine whether they have the right parts at each assembly or disassembly step. This is crucial for ensuring successful completion without missing any steps or parts. The machine learning prototype aims to detect and identify the LEGO parts using camera input, enhancing the building experience.

In conclusion, the AISCV CV LEGO server acts as a bridge between the user's computer vision system and the data required to assemble or disassemble the LEGO robot, making the process efficient and user-friendly.

# Setup

## Create Environment

### Navigate to server directory

```console
cd server
```

### Create Conda Environment

```console
conda create --name aisscv_oracle --file requirements.txt
```

# Initialize database

If `lego.db` exists, there is no need to re-initialize the database.

```console
python3 initialize_db.py
```

# Run server

## Through python

```console
python3 app.py
```

## Through flask

```console
export FLASK_APP=server
```

```console
export FLASK_ENV=development
```

```console
flask run
```

If this error occurs:

```console
Error: The file/path provided (server) does not appear to exist.  Please verify the path is correct.  If app is not on PYTHONPATH, ensure the extension is .py
```

then:

```console
export PYTHONPATH=$PYTHONPATH:/path/to/your/file
```
